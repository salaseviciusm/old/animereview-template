# HTML5/CSS3 Anime related website template
Screenshots:
* [Screenshot of index page](screenshot-index.png)
* [Screenshot of contact page](screenshot-contact.png)
* [Screenshot of categories page](screenshot-categories.png)
* [Screenshot of article page](screenshot-article.png)
* [Screenshot of review page](screenshot-review.png)

## Built With

HTML5/CSS3 <br>
jQuery

## Authors

* **Mantas Salasevicius** - *Developer* - [Portfolio](https://mantas.dev), [Gitlab](https://gitlab.com/mantas.dev)

## Date of development

2017

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details